package hbase;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HBaseLab {

    // Instante Configuration object
    private static Configuration conf;

    // constructor that create the configuration
    public HBaseLab(){
        conf = HBaseConfiguration.create();
    }

    /**
     * createTheTable function
     * The table will have a row id and two column families : "Info" and "Friends"
     * @param tableName - name of the table
     * @param columnFamilies - name of the array of column family
     * @throws Exception
     * @return void
     */
    public void createTheTable(String tableName, String[] columnFamilies) throws Exception{
        HBaseAdmin admin = new HBaseAdmin(conf); // give access to the database
        if (!admin.tableExists(tableName)) {  // check if the table exists. If not, it creates the table
            HTableDescriptor tableDesc = new HTableDescriptor(tableName); // contains the details about the HBase table
            for (int i = 0; i<columnFamilies.length; i++){
                tableDesc.addFamily(new HColumnDescriptor(columnFamilies[i])); // Add column family
            }
            admin.createTable(tableDesc); // creating the tables with its structures
            System.out.println("Creation of the table " + tableName + ". Done.");
        } else {
            System.out.println("Table already exists. Not recreated.");
        }
    }


    /**
     * addRow function
     * @param  tableName    name of the table
     * @param  rowKey       first name (row id) in which we want to add infos and friends
     * @param  columnFamily column family we want to add
     * @param  qualifier    the "key" of the value we want to add
     * @param  value        The value which contains the informations we want to add
     * @throws Exception
     * @return void
     */
    public void addRow(String tableName, String rowKey, String columnFamily, String qualifier, String value) throws Exception {
        try {
            HTable table = new HTable(conf, tableName); // Give access to the table
            Put put = new Put(Bytes.toBytes(rowKey)); // Insert row id
            put.add(Bytes.toBytes(columnFamily), Bytes.toBytes(qualifier), Bytes.toBytes(value)); // insert for a column family, a key and its value
            table.put(put); // it puts it in the table
            System.out.println("Insert record "+ rowKey +" to table " + tableName + ". Done.");
        }catch (IOException e){
            e.printStackTrace();
        }
    }



    /**
     * addRow function
     * @param  tableName    name of the table
     * @param  rowKey       first name (row id) in which we want to add infos and friends
     * @param  columnFamily column family we want to add
     * @param  qualifier    the "key" of the value we want to add
     * @param  value        The value which contains the informations we want to add
     * @throws Exception
     * @return void
     */
    public void addRow(String tableName, String rowKey, String columnFamily, String qualifier, List<String> value) throws Exception {
        try {
            // convert the array of other friends to string in order to store it in the table
            String others = "";
            for (String v : value){
                others = others.concat(v +" ");
            }
            HTable table = new HTable(conf, tableName); // Give access to the table
            Put put = new Put(Bytes.toBytes(rowKey)); // Insert row id
            put.add(Bytes.toBytes(columnFamily), Bytes.toBytes(qualifier), Bytes.toBytes(others)); // insert for a column family, a key and its value
            table.put(put); // it puts it in the table
            System.out.println("Insert record "+ rowKey +" to table " + tableName + ". Done.");
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    /**
     * printAllRows
     * @param tableName - name of the table
     * @return void
     */
    public void printAllRows (String tableName){
        try {
            HTable table = new HTable(conf, tableName); //access to the table
            Scan s = new Scan(); // Scan objet that will be used to read in the table
            ResultScanner resultScan = table.getScanner(s); // Results are stored as array in resultScan
            for (Result r : resultScan){ // for each result, it prints the value
                for(KeyValue kv : r.raw()){
                    System.out.print(new String(kv.getRow()) + " ");
                    System.out.print(new String(kv.getFamily()) + ":");
                    System.out.print(new String(kv.getQualifier()) + " ");
                    System.out.println(new String(kv.getValue()));
                }
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * main function
     * @param args
     * The main function is a REPL that will be used to fill the table "Name".
     * At the end of the REPL, the data stored in the table are printed.
     */
    public static void main (String[] args){
        try {
            HBaseLab hBase = new HBaseLab(); // Creation of the class

            // Creation of the table if it does not exist
            String tableName = "Name";
            String columnFamily1 = "Info";
            String columnFamily2 = "Friends";
            String[] columnFamilies = { columnFamily1, columnFamily2 };
            hBase.createTheTable(tableName,columnFamilies); // Create the table with its name and its column families.

            // REPL
            boolean quit = false;
            do {
                Scanner sc = new Scanner(System.in); // Using the Scanner Object to read in the standard entry

                System.out.println("Enter a first name."); // Ask the user to enter a name
                String firstName = sc.nextLine(); // Read the line and store it as first name
                System.out.println("Enter " + firstName +"'s gender."); // Ask the user to enter a gender
                String gender = sc.nextLine();  // Read the line and store it as gender
                hBase.addRow(tableName, firstName, columnFamily1, "Gender", gender);  // insert the gender information in the table for the corresponding row id and column family
                System.out.println("Enter " + firstName +"'s date of birth."); // Ask the user to enter a date of birth
                String dateOfBirth = sc.nextLine(); // Read the line and store it as date of birth
                hBase.addRow(tableName, firstName, columnFamily1, "Date of birth", dateOfBirth); // insert the date of birth information in the table for the corresponding row id and column family
                System.out.println("Enter " + firstName +"'s phone number."); // Ask the user to enter a phone number
                String phoneNumber = sc.nextLine(); // Read the line and store it as phone number
                hBase.addRow(tableName, firstName, columnFamily1, "Phone number", phoneNumber); // insert the phone number information in the table for the corresponding row id and column family
                System.out.println("Enter " + firstName +"'s email address."); // Ask the user to enter a phone number
                String email = sc.nextLine(); // Read the line and store it as email
                hBase.addRow(tableName, firstName, columnFamily1, "Email", email); // insert the email information in the table for the corresponding row id and column family
                String bff = ""; //
                do {
                    System.out.println("Enter a best friend forever (mandatory)");
                    bff = sc.nextLine();
                } while(bff.equals("")); // check if the "bff" is empty or not, re-ask until it is not (as it is mandatory)
                hBase.addRow(tableName, firstName, columnFamily2, "BFF", bff); // insert the BFF information in the table for the corresponding row id and column family
                List<String> others = new ArrayList<String>(); //list to store the others friends
                System.out.println("Add another friend ? Enter 'yes' if you want, anything else if you don't");
                String add = sc.nextLine();
                // loop until the user do not want to enter a friend anymore
                while (add.equals("yes")) {  // if the user enter "yes", it keep asking for a friend
                    System.out.println("Enter the first name of a friend");
                    others.add(sc.nextLine()); //store the friend in a list of other friends
                    System.out.println("Add another friend ? Enter 'yes' if you want, anything else if you don't");
                    add = sc.nextLine();
                }
                hBase.addRow(tableName, firstName, columnFamily1, "Others", others); // after the loop the other friends are stored in the table
                System.out.println("Do you want to register another person/row ? Enter 'yes' to continue, anything else to quit.");
                String q = sc.nextLine();
                if (!q.equals("yes")){ // if user enter yes, he/she wants to quit
                    quit = true; // quit set to true to get out of the loop
                }
            } while (!quit); // loop until the user quit
            hBase.printAllRows(tableName); // print all the  data from the table
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}